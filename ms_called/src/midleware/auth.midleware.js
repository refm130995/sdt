'use strict';

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'promo$clave';

const GlobalAuth = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(403).send({
            message: 'La peticion no tiene la cabecera de autenticacion'
        });
    }
    var token = req.headers.authorization.replace(/['"]+/g, '');
    try {
        var payload = jwt.decode(token, secret);
        if (payload.exp <= moment().unix) {
            return res.status(401).send({ message: 'El token a expirado' });
        }
    } catch (ex) {
        console.log(ex);
        return res.status(404).send({ message: 'token no valido' });
    }
    req.user = payload;

    next();
};

const AdminAuth = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(403).send({
            message: 'La peticion no tiene la cabecera de autenticacion'
        });
    }
    var token = req.headers.authorization.replace(/['"]+/g, '');
    try {
        var payload = jwt.decode(token, secret);
        if (payload.role !== 'administrador') {
            return res
                .status(404)
                .send({ message: 'No tiene permisos necesarios' });
        }
        if (payload.exp <= moment().unix) {
            return res.status(401).send({ message: 'El token a expirado' });
        }
    } catch (ex) {
        console.log(ex);
        return res.status(404).send({ message: 'token no valido' });
    }
    req.user = payload;

    next();
};
module.exports = {
    GlobalAuth,
    AdminAuth
};
