'use strict';

const CalleController = require('../app/controllers/called.controller');

class socket {
    constructor(socket) {
        this.io = socket;
    }

    socketEvents() {
        this.io.on('connect', socket => {
            socket.on('attention', async data => {
                const res = await rescalled(data);
                this.io.emit('attend', res);
            });
        });
    }
}

async function rescalled(data) {
    var e = await CalleController.CreateTailOrTicket(data);
    return e;
}
module.exports = socket;
