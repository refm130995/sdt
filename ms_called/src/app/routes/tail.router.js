'use strict';

const TailController = require('../controllers/tail.controller');
const Auth = require('../../midleware/auth.midleware');
const express = require('express');
const api = express.Router();

api.post('/tail', TailController.CreateTailOrTicket);
api.get('/tail', TailController.GetTail);
api.delete('/tail', TailController.DeleteTail);

api.get('/ticket', TailController.GetTicketsOfUser);
api.delete('/ticket', TailController.DeleteTicket);
api.put('/ticket', TailController.UpdateStatusTicket);

module.exports = api;
