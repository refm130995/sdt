'use strict';

const mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');
const Schema = mongoose.Schema;

const CalledSchema = Schema({
    _id: {
        type: String,
        unique: true
    },
    branch: String,
    tickets_attention: [
        {
            ticket: String,
            agent: String
        }
    ]
});

module.exports = mongoose.model('Called', CalledSchema);
