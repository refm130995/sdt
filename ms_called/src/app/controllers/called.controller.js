'use strict';

const Called = require('../models/called.model');
const uuidv4 = require('uuid/v4');
/*
 * CREA LA COLA DE atencion SI NO EXISTE Y GUARDA LOS TICKETS DENTRO DEL MISMO
 */
async function CreateTailOrTicket(data) {
    try {
        const branch = data.branch;
        const findTailBranch = await Called.findOne({ branch: branch }).exec();

        if (!findTailBranch) {
            /* Si no existe la cola, la crea y guarda el primer ticket en esta */
            const tailBranch = new Called();
            tailBranch._id = uuidv4();
            tailBranch.branch = branch;
            tailBranch.tickets_attention = [
                {
                    agent: data.agent,
                    ticket: data.ticket
                }
            ];
            await tailBranch.save();
            return tailBranch;
        } else {
            const array = await findExistTicket(
                findTailBranch.tickets_attention,
                {
                    agent: data.agent,
                    ticket: data.ticket
                }
            );
            console.log('despues de buscar', array);
            findTailBranch.tickets_attention = array;
            const tailBranch = await Called.findOneAndUpdate(
                { _id: findTailBranch._id },
                findTailBranch
            ).exec();
            return findTailBranch;
        }
    } catch (e) {
        return e;
    }
}

async function findExistTicket(array, data) {
    if (array.length > 0) {
        var results = array.filter(function(ticket, i) {
            return ticket.agent === data.agent;
        });
        console.log('resultado', results);
        if (results.length > 0) {
            var results2 = array.filter(function(ticket, i) {
                return ticket.agent != results[0].agent;
            });
            return results2;
        } else {
            array.push(data);
            console.log('agrego ', array);
            return array;
        }
    } else {
        array = [data];
        return array;
    }
}
/*
 *   ELIMINA POR COMPLETO UNA COLA EXISTENTE
 *   query:
 *       service: uuid del servicio
 */
/* async function DeleteTail(req, res) {
    try {
        const taildelete = await Called.findOneAndDelete({
            service: req.query.service
        }).exec();
        if (taildelete) {
            res.status(300).send({ tail: taildelete });
        }
    } catch (e) {
        res.status(500).send({ error: e });
    }
} */

/*
 *   RETORNA TODA LA INFORMACION DE UNA COLA Y LOS TICKETS REGISTRADOS
 *   query:
 *       service: id del servicio
 */
/* async function GetTail(req, res) {
    try {
        const tail = await Called.findOne({ service: req.query.service })
            .populate('tail')
            .exec();
        if (tail) {
            res.status(300).send({ tail: tail });
        }
    } catch (e) {
        res.status(500).send({ error: e });
    }
} */

module.exports = {
    CreateTailOrTicket
};
