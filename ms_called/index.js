const mongoose = require('mongoose');
const app = require('./app');
const port = process.env.PORT || 8000;
const socketEvent = require('./src/config/socket');
//CONEXION A LA BASE DE DATOS
mongoose.connect(
    'mongodb://localhost:27017/called-db',
    { useNewUrlParser: true, useFindAndModify: false },
    (err, res) => {
        if (err) {
            throw err;
        } else {
            console.log('Conexion a la base de datos establecida');
            var server = app.listen(port, function() {
                console.log(
                    'Servidor escuchando por el puerto http://localhost:' + port
                );
            });

            var io = require('socket.io')(server);
            var socket = new socketEvent(io);
            socket.socketEvents();
        }
    }
);
