import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';


if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./firebase-messaging-sw.js")
    .then(function(registration) {
      console.log("Registration successful, scope is:", registration.scope);
      navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
    })
    .catch(function(err) {
      console.log("Service worker registration failed, error:", err);
    });
}


ReactDOM.render(<App />, document.getElementById('root'));

